var express = require('express');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var Storage = {
  add: function(name) {
    var item = {name: name, id: this.setId};
    this.items.push(item);
    this.setId += 1;
    return item;
  },
  delete: function(id) {
    if (!this.items[id-1]) return false;
    this.items[id-1] = null;
    return true;
  },
  update: function(id, value) {
    if (!this.items[id-1]) return false;
    this.items[id-1].name = value;
    return this.items[id-1];
  },
  getItemById: function(id) {
    return this.items.filter(function(obj) {
      return obj.id == id;
    });
  }
};

var createStorage = function() {
  var storage = Object.create(Storage);
  storage.items = [];
  storage.setId = 1;
  return storage;
}

var storage = createStorage();

storage.add('Broad beans');
storage.add('Tomatoes');
storage.add('Peppers');

var app = express();
app.use(express.static('public'));

app.get('/items', function(request, response) {
    response.json(storage.items);
});

app.get('/items/:id', function(request, response) {
    response.json(storage.getItemById(request.params.id));
});

app.delete('/items/:id', function(request, response) {
  if (storage.delete(request.params.id)) {
    return response.sendStatus(200);
  }
  return response.sendStatus(400);
});

app.put('/items/:id', jsonParser, function(request, response) {
  if (request.params.id > storage.items.length) {
    return response.sendStatus(400);
  }
  if (parseInt(request.params.id, 10) != parseInt(request.body.id, 10)) {
    return response.sendStatus(400);
  }
  if (!('name' in request.body)) {
      return response.sendStatus(400);
  }
  var item = storage.update(request.params.id, request.body.name);
  response.status(200).json(item);
});

app.post('/items', jsonParser, function(request, response) {
    if (!('name' in request.body)) {
        return response.sendStatus(400);
    }

    var item = storage.add(request.body.name);
    response.status(201).json(item);
});

app.listen(process.env.PORT || 8080, process.env.IP);

exports.app = app;
exports.storage = storage;
